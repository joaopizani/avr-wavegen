/*
 * File: samples.h
 *
 * Author: João Paulo Pizani Flor
 * Date: 2017-10-09
 *
 */
#ifndef SAMPLES_H
#define SAMPLES_H

#include <stdint.h>


#define CARRIER_FREQ 1143120


#define SINE880_FREQ 880
#define SINE880_COUNTER_PRESCALER 1
#define SINE880_PDM_SIZE ( CARRIER_FREQ / SINE880_FREQ )

uint8_t sine880_pdm[SINE880_PDM_SIZE];

