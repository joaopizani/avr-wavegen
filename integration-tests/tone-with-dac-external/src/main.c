#include <avr/interrupt.h>
#include <avr-alarm/alarm.h>
#include <avr-tone/avr-tone.h>

/*
 * Test program for the avr-tone library using a buzzer as backend.
 */

// By using the SHORT_BEEPS define, we can have shorter tones for easier simulation
#define BEEP_T 10
#ifdef SHORT_BEEPS
#undef BEEP_T
#define BEEP_T 1
#endif


// frequencies and durations used in the demo song
#define PA 0 //Hz

#define C4 261 //Hz
#define D4 293 //Hz
#define E4 329 //Hz
#define F4_ 369 //Hz
#define G4 391 //Hz
#define A4 440 //Hz
#define B4 493 //Hz

#define C5 523 //Hz
#define D5 587 //Hz
#define E5 659 //Hz
#define F5_ 739 //Hz
#define G5 783 //Hz
#define A5 880 //Hz
#define B5 987 //Hz

#define C6 1046 //Hz

#define T8 (40 * BEEP_T) //ms
#define T4 (80 * BEEP_T) //ms
#define T2 (160 * BEEP_T) //ms


void het_wilhelmus(void) {
    for(int i = 0; i < 2; ++i) {
        t_(D4,T4); t_(G4,T4); t_(G4,T4); t_(A4,T8); t_(B4,T8); t_(C5,T8); t_(A4,T8); t_(B4,T4);
        t_(A4,T8); t_(B4,T8); t_(C5,T4); t_(B4,T4); t_(A4,T8); t_(G4,T8); t_(A4,T4); t_(G4,T2); t_(PA,T4);
    }

    t_(B4,T8); t_(C5,T8); t_(D5,T2); t_(E5,T4); t_(D5,T2); t_(C5,T4); t_(B4,T4);
    t_(A4,T8); t_(B4,T8); t_(C5,T4); t_(B4,T4); t_(A4,T4); t_(G4,T4); t_(A4,T2); t_(PA,T4);

    t_(D4, T4); t_(G4, T8); t_(F4_, T8); t_(G4, T8); t_(A4, T8); t_(B4, T4); t_(A4, T2); t_(G4, T4);
    t_(F4_, T4); t_(D4, T4); t_(E4, T8); t_(F4_, T8); t_(G4, T4); t_(G4, T4); t_(F4_, T4); t_(G4, T2); t_(PA, T2);
}

int main(void) {
    tone_init();
    tone_start();
    tone_setsample_defaultsquare();

    sei();
    het_wilhelmus();

    while(1) {
        het_wilhelmus();
    }

    return 0;
}

