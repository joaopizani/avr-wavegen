AVR-Buzzer: Plays a digital 1-bit signal through a buzzer
=========================================================

This library could not be simpler, really ☺  It has only one source file, with less than 10 lines of code.
It defines an *interrupt service routine* (ISR) that handles the compare match A for TIMER2 (8-bit)
of an MCU in the atmega168 series. The same ISR works for the atmega328{p} as well.

This library is the simplest possible **LAYER0**  in a sound generation and transmission stack.
A digital, 1-bit signal is assumed to be given to this library as input, with type `uint8_t`.
The value `0` in decimal is interpreted as a binary 0 (OFF) while anything else is interpreted as 1 (ON).

The bitstream given to this library will be output on PORTB0 (pin 0 of PORTB), with precise and uniform timing.
In order to control the working of this library, the client has to set three parameters:

 * Pointer to a buffer from which the samples are to be read (type: `uint8_t*`)
 * Size of this buffer; reads from the buffer will proceed circularly between `buf` and `buf + size` (type: `uint8_t`)
 * Frequency with which to read from `buf[cnt++]` (type: `uint16_t`)

This library can be used for the output of any bitstream with uniform and precise timing.
However there are two particularly common use cases:

 * PDM signals (Pulse-Density Modulated)
 * Simple square waves (the input stream follows the regular expression `(01)*`)

In the case of square waves, a fixed buffer is used of size 2, with contents equal to `01`,
and for this case (square waves), a simplified `setup` function is available for convenience.


Installing and using in applications
------------------------------------

As with all other component libraries of [avr-wavegen](https://github.com/joaopizani/avr-wavegen),
you can compile and install avr-buzzer at a certain PREFIX in your filesystem by just typing:

```bash
make install PREFIX=[prefix]
```

To use avr-buzzer as part of an application, you must add avr-buzzer's headers to the compiler's include path,
and link your application with the static lib file `libavr-buzzer.a` found under `$PREFIX/lib`.
In case you are using a Makefile from `avr-wavegen` as template for the Makefile in your application
or using the Makefile template from [avr-utils](https://github.com/joaopizani/avr-utils)
then you can just add the following to your `paths.def`:

``` bash
AVRBUZZER_ROOT=[path to PREFIX where you installed avr-buzzer]
AVRBUZZER_INCLUDE=${AVRBUZZER_ROOT}/include
AVRBUZZER_LIBS=${AVRBUZZER_ROOT}/lib/avr-buzzer
AVRBUZZER_INCFLAGS=-I${AVRBUZZER_INCLUDE}
AVRBUZZER_LIBFLAGS=-Wl,--whole-archive -L${AVRBUZZER_LIBS} -lavr-buzzer -Wl,--no-whole-archive
```

Observation: the `-Wl,--whole-archive` and `-Wl,--no-whole-archive` options are needed because the library
mainly consists of an interrupt handler, which wouldn't otherwise be linked in.
Finally, you should append `AVRBUZZER_INCFLAGS` and `AVRBUZZER_LIBFLAGS`, respectively,
to the variables `EXT_INCFLAGS` and `EXT_LIBFLAGS` present at the end of your paths.def.
Like this:

``` bash
EXT_INCFLAGS=${LIB1_INCFLAGS} ${LIB2_INCFLAGS} ${AVRBUZZER_INCFLAGS} ...
EXT_LIBFLAGS=${LIB1_LIBFLAGS} ${LIB2_LIBFLAGS} ${AVRBUZZER_LIBFLAGS} ...
```

