/*
 * File: dac_external.h
 *
 * Author: João Paulo Pizani Flor
 * Date: 2017-10-09
 *
 */
#ifndef DAC_EXTERNAL_H
#define DAC_EXTERNAL_H

#include <stdint.h>
#include <avr/io.h>


#define DAC_SAMPLES_COUNTER_PRESCALER 1
#define DAC_SAMPLES_COUNTER_BASE_FREQ ( F_CPU / DAC_SAMPLES_COUNTER_PRESCALER )


void __dac_start_dontcarefreq(void);


void dac_init(void);

void dac_start(uint32_t f_Hz);

void dac_stop(void);

void dac_setsamplebuf(uint8_t* sample_buf, uint16_t sample_bufsize);

void dac_setfreq(uint32_t f_Hz);


#endif /* DAC_EXTERNAL_H */

