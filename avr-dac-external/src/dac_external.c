/*
 * File: dac_external.c
 *
 * Author: João Paulo Pizani Flor
 * Date: 2017-10-09
 *
 */
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr-utils/timers-atmega328p.h>
#include "dac_external.h"


static uint16_t __dac_bufsize = 1;
static uint8_t *__dac_buffer = 0;

static uint16_t __dac_bufcnt = 0;


void __dac_start_dontcarefreq(void) {
    TCNT2 = 0x00;
    //TODO: prescaler value from desired frequency
    TCCR2B |= getTimer2PrescaleBits_atmega328p(DAC_SAMPLES_COUNTER_PRESCALER);
}


void dac_init(void) {
    // init samples counter
    TCCR2A |= _BV(WGM21); // clear on compare match
    TIMSK2 |= _BV(OCIE2A); // enable compare match intr.

    __dac_bufcnt = 0;

    // init output
    DDRB = 0x00;
    PORTB = 0x00;
}

void dac_start(uint32_t f_Hz) {
    OCR2A = DAC_SAMPLES_COUNTER_BASE_FREQ / f_Hz; // TODO: prescaler value from desired frequency
    __dac_start_dontcarefreq();
}

void dac_stop(void) {
    TCCR2B ^= ~ ( getTimer2PrescaleBits_atmega328p(DAC_SAMPLES_COUNTER_PRESCALER) );
    PORTB = 0x00;
}

void dac_setsamplebuf(uint8_t* sample_buf, uint16_t sample_bufsize) {
    dac_stop();

    __dac_bufcnt = 0;
    __dac_bufsize = sample_bufsize;
    __dac_buffer = sample_buf;

    __dac_start_dontcarefreq();
}

void dac_setfreq(uint32_t f_Hz) {
    dac_stop();
    __dac_bufcnt = 0;
    dac_start(f_Hz);
}


ISR(TIMER2_COMPA_vect) {
    // Direct write to the PORT, DAC is done EXTERNALLY.
    // In the internal case, we write (with dithering) to PWM counter compare reg
    PORTB = __dac_buffer[__dac_bufcnt];

    __dac_bufcnt = (__dac_bufcnt + 1) % __dac_bufsize;
}

