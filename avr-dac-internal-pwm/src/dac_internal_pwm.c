/*
 * File: dac_internal_pwm.c
 *
 * Author: João Paulo Pizani Flor
 * Date: 2017-10-15
 *
 */
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "dac_internal_pwm.h"


static uint16_t __dac_bufsize = 1;
static uint8_t *__dac_buffer = 0;

static uint16_t __dac_bufcnt = 0;


void __dac_init_pwm(void) {
    TCCR0A |= _BV(WGM00) | _BV(WGM01); // Fast PWM mode, TOP source is OCR0A
    TCCR0B |= _BV(WGM02);

    TCCR0A |= _BV(COM0A1) | _BV(COM0B1); // Non-inverting mode for OC0B, also OC0A

    DDRD |= _BV(PD5); // OC0B output
    PORTD = 0x00;
}

void __dac_start_pwm_dontcarefreq(void) {
    TCNT0 = 0x00
    TCCR0B |= getTimer0PrescaleBits_atmega328p(DAC_PWM_COUNTER_PRESCALER);
}

void __dac_start_pwm(int is250KHz) {
    uint8_t top = 255;
    if(is250KHz) { top = 64; }
    OCR0A = top;

    __dac_start_pwm_dontcarefreq();
}

void __dac_stop_pwm(void) {
    TCCR0B ^= ~ ( getTimer0PrescaleBits_atmega328p(DAC_PWM_COUNTER_PRESCALER) );
    PORTD = 0x00;
}



void __dac_start_dontcarefreq(void) {
    TCNT2 = 0x00;
    //TODO: prescaler value from desired frequency
    TCCR2B |= getTimer2PrescaleBits_atmega328p(DAC_SAMPLES_COUNTER_PRESCALER);

    __dac_start_pwm(1);
}


void dac_init(void) {
    // init samples counter
    TCCR2A |= _BV(WGM21); // clear on compare match
    TIMSK2 |= _BV(OCIE2A); // enable compare match intr.

    __dac_bufcnt = 0;

    // init output
    __dac_init_pwm();
}

void dac_start(uint32_t f_Hz) {
    OCR2A = DAC_SAMPLES_COUNTER_BASE_FREQ / f_Hz; // TODO: prescaler value from desired frequency
    __dac_start_dontcarefreq();
}

void dac_stop(void) {
    TCCR2B ^= ~ ( getTimer2PrescaleBits_atmega328p(DAC_SAMPLES_COUNTER_PRESCALER) );

    __dac_stop_pwm();
}

void dac_setsamplebuf(uint8_t* sample_buf, uint16_t sample_bufsize) {
    dac_stop();

    __dac_bufcnt = 0;
    __dac_bufsize = sample_bufsize;
    __dac_buffer = sample_buf;

    __dac_start_dontcarefreq();
}

void dac_setfreq(uint32_t f_Hz) {
    dac_stop();
    __dac_bufcnt = 0;
    dac_start(f_Hz);
}


ISR(TIMER2_COMPA_vect) {
    // Using OCR0B as length of duty cycle relative to TOP (OCR0A)
    // TODO: now just truncating to 6 bit, later we should do dithering (maybe)
    OCR0B = __dac_buffer[__dac_bufcnt] >> 2;

    __dac_bufcnt = (__dac_bufcnt + 1) % __dac_bufsize;
}

