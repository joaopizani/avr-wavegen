/*
 * File: dac_internal_pwm.h
 *
 * Author: João Paulo Pizani Flor
 * Date: 2017-10-15
 *
 */
#ifndef DAC_INTERNAL_PWM_H
#define DAC_INTERNAL_PWM_H

#include <stdint.h>
#include <avr/io.h>


#define DAC_SAMPLES_COUNTER_PRESCALER 1
#define DAC_SAMPLES_COUNTER_BASE_FREQ ( F_CPU / DAC_SAMPLES_COUNTER_PRESCALER )


void __dac_start_dontcarefreq(void);


void dac_init(void);

void dac_start(uint32_t f_Hz);

void dac_stop(void);

void dac_setsamplebuf(uint8_t* sample_buf, uint16_t sample_bufsize);

void dac_setfreq(uint32_t f_Hz);



#define DAC_PWM_COUNTER_PRESCALER 1

void __dac_init_pwm(void);

void __dac_start_pwm_dontcarefreq(void);

void __dac_setfreq_pwm(int is250KHz);

void __dac_stop_pwm(void);


#endif /* DAC_INTERNAL_PWM_H */

