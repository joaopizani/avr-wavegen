#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <avr-utils/timers-atmega328p.h>
#include <avr-alarm/alarm.h>
#include <avr-dac-external/dac_external.h>
#include "avr-tone.h"


#define TONE_DEFAULT_SAMPLEBUF_SIZE 2

static uint8_t __tone_default_samplebuf[TONE_DEFAULT_SAMPLEBUF_SIZE] = { 0 , UINT8_MAX };


void __tone_setfreq_alarm_handler(void* freq_uint16_ptr) {
    uint16_t* f_Hz_ptr = (uint16_t*) freq_uint16_ptr;
    uint16_t f_Hz = *f_Hz_ptr; // saving

    if(f_Hz == 0) {
        dac_stop();
    } else {
        dac_setfreq(f_Hz * 2);
        free(f_Hz_ptr); // we don't need it anymore
    }
}


void tone_init(void) {
    alarm_init(TONE_TIME_RESOLUTION);
    dac_init();
}

void tone_start(void) {
    alarm_timer_start();
    dac_start(440); // TODO: magic number, change later to sampling rate
}

void tone_setsample(uint8_t* sample_buf, uint16_t sample_bufsize) {
    dac_setsamplebuf(sample_buf, sample_bufsize);
}

// TODO: remove this later, when we are using sample banks
void tone_setsample_defaultsquare(void) {
    dac_setsamplebuf(__tone_default_samplebuf, TONE_DEFAULT_SAMPLEBUF_SIZE);
}

void t_(uint16_t f_Hz, uint16_t dur_ms) {
    static uint16_t zero_Hz = 0;
    static uint16_t* zero_Hz_ptr = &zero_Hz;

    uint16_t* f_Hz_ptr = malloc(sizeof(uint16_t));
    *f_Hz_ptr = f_Hz;

    time_ms_t t = alarm_until_last_deadline();
    alarm_insert(t, __tone_setfreq_alarm_handler, f_Hz_ptr); // beginning of the sound
    alarm_insert(t + dur_ms, __tone_setfreq_alarm_handler, zero_Hz_ptr); // silence thereafter
}

