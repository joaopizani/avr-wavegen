/*
 * File: avr-tone.h
 *
 * Author: João Paulo Pizani Flor
 * Date: 2017-10-09
 *
 */
#ifndef AVRTONE_H
#define AVRTONE_H

#include <stdint.h>

/** time resolution of tone generation, in miliseconds. The library cannot distinguish between two durations
 * with a difference less than TONE_TIME_RESOLUTION. For example, if TONE_TIME_RESOLUTION = 10, then
 * playTone(440, 500) and playTone(440, 502) will have the same duration.
 */
#define TONE_TIME_RESOLUTION 10



void __tone_setfreq_alarm_handler(void* freq_uint16_ptr);



void tone_init(void);

void tone_start(void);

void tone_setsample(uint8_t* sample_buf, uint16_t sample_bufsize);

void tone_setsample_defaultsquare(void);

void t_(uint16_t f_Hz, time_ms_t dur_ms);


#endif /* AVRTONE_H */
